import { createRouter, createWebHistory } from "vue-router";
import App from './App.vue';

import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'

import Hoteles from './components/Hoteles.vue'
import Habitaciones from './components/Habitaciones.vue'
import Reserva from './components/Reserva.vue'
import Administrador from './components/Administrador.vue'

const routes = [
  {
    path: '/',
    name: 'root',
    component: App
  },
  {
    path: '/user/administrador',
    name: 'administrador',
    component: Administrador
  },
  {
    path: '/user/logIn',
    name: "logIn",
    component: LogIn
  },
  {
    path: '/user/signUp',
    name: "signUp",
    component: SignUp
  },
  {
    path: '/user/hoteles',
    name: "hoteles",
    component: Hoteles
  },
  {
    path: '/user/habitaciones/:id',
    name: "habitaciones",
    component: Habitaciones
  },
  {
    path: '/user/reserva',
    name: "reserva",
    component: Reserva
  }
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;